var serviceURL = "http://tawornyangyont.net/services/";
// var serviceURL = "http://localhost/thawornyangyont/services/";

//for welcome message
var username;

//for dropdown list items
var brandlist;
var typelist;
var sizelist;

//for filter results
var keyword;
var brand;
var type;
var size;
var in_stock;

var is_first_time_load = true;


$('#page-main-menu').bind('pageinit', function(event) {
	//$('#body-results').hide();
	username = localStorage.getItem("username");
	$('h3#txt_welcome').text('ยินดีต้อนรับ ' + username);
	getBrandList();
	getTypeList();
	getSizeList();
	getColumns();
	
});

//////////////////////////////
$("#btn-search").click(function(){
	//alert ("btn-search clicked");
	getProductList();
});
////////////////////////////////

//get unique brands from database
function getBrandList() {
	$.getJSON(serviceURL + 'getbrandlist.php', function(data) {
		//alert ("in getJSON");
		//$('#select-native-brand option').remove();
		brandlist = data.items;
		$.each(brandlist, function(index, brand) {
			$('#select-native-brand').append('<option value =' + brand.brand + '>' + brand.brand + '</option>' );
		});
		$('#select-native-brand').selectmenu('refresh');
	});
}

//get unique types from database
function getTypeList() {
	$.getJSON(serviceURL + 'gettypelist.php', function(data) {
		//alert ("in getJSON");
		//$('#select-native-type option').remove();
		typelist = data.items;
		$.each(typelist, function(index, type) {
			$('#select-native-type').append('<option value =' + type.type + '>' + type.type + '</option>' );
		});
		$('#select-native-type').selectmenu('refresh');
	});
}

//get unique sizes from database
function getSizeList() {
	$.getJSON(serviceURL + 'getsizelist.php', function(data) {
		//alert ("in getJSON");
		//$('#select-native-size option').remove();
		sizelist = data.items;
		$.each(sizelist, function(index, size) {
			$('#select-native-size').append('<option value =' + size.size + '>' + size.size + '</option>' );
		});
		$('#select-native-size').selectmenu('refresh');
	});
}

function getColumns(callBack){
	var username = localStorage.username;
	// var detail = $("#textinput-detail:selected" ).text();
	// var brand = $("#select-native-brand" ).val();
	// var type = $("#select-native-type" ).val();
	// var size = $("#select-native-size" ).val();
	// var in_stock = $("#flip-instock" ).val();

	//var dataString="username="+username+"&detail="+detail+"&brand="+brand+"&type="+type +"&size="+size+"&in_stock="+in_stock;	
	var dataString="username="+username;
	console.log(dataString);
	$.ajax({
		type: "POST",
		url: serviceURL + 'getcolumns.php',
		data: dataString,
		crossDomain: true, 
		cache: false,
		success: function(data){
			// console.log("getColumns success");
			//console.log(data);
			//$('#body-results').empty();
			// console.log(data);
			$('#body-results').html(data);
			//$("page-main-menu").trigger('create');
			//alert("to refresh");
			$("#body-results").trigger('create');
			//$("#page-main-menu").trigger('create');
			//$("table#body-results").table("refresh");
			//$('#table-results').tablesorter();
			//$('#body-results').show();
			//$.mobile.hidePageLoadingMsg();
			
		}
	});
	
}
function getProductList(){
	var username = localStorage.username;
	var detail = $("#textinput-detail:selected" ).text();
	var brand = $("#select-native-brand" ).val();
	var type = $("#select-native-type" ).val();
	var size = $("#select-native-size" ).val();
	var in_stock = $("#flip-instock" ).val();

	var dataString="username="+username+"&detail="+detail+"&brand="+brand+"&type="+type +"&size="+size+"&in_stock="+in_stock;	
	console.log(brand);
	console.log(dataString);
	$.ajax({
		type: "POST",
		url: serviceURL + 'getproductlist.php',
		data: dataString,
		crossDomain: true,
		cache: false,
		beforeSend: function(){ 
			$.mobile.showPageLoadingMsg(); 
			$('#body-results').hide();
		},
		success: function(data){
			console.log("getProductList success");
			//console.log(data);
			//$('#body-results').empty();
			// console.log(data);
			//var tbody = $('#table-results-tbody');
			$('#table-results > tbody:last').empty();
			$('#table-results > tbody:last').append(data).trigger( "create" );;
			//$('#table-results-tbody').html(data);
        	//$("#table-results").trigger('create');
			//$('#body-results').append(data);
			//console.log($('#table-results').html());
			//$("page-main-menu").trigger('create');
			//alert("to refresh");
			//$("#body-results").trigger('create');
			$("#table-results").table("refresh");
			//$("#page-main-menu").trigger('create');
			//$("#table-results").table("create");
			$('#table-results').tablesorter();
			$('th.nosort').unbind('click');
			$('#body-results').show();
			$.mobile.hidePageLoadingMsg();
			
		}
	  });	 
}
function callMeWhenDone() {
	// console.log("finished");
}

$("#btn-logout").click(function(){
	localStorage.clear();
	window.location.href = "index.html";
});
	
//doLongProcessWithCallback("stringInput", 34, callMeWhenDone);
	




